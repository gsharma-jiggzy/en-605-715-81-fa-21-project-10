#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
  
Adafruit_BNO055 bno = Adafruit_BNO055(55);
byte imu_data[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
 
// IMU Variables used for digit extraction
int roll = 0;
int pitch = 0;
int yaw = 0;
int SLAVE_ADDRESS = 0X04;
/**************************************************************************/
/*
    Retrieves the digit of any position in an integer. The rightmost digit
    has position 0. The second rightmost digit has position 1, etc.
    e.g. Position 3 of integer 245984 is 5.
*/
/**************************************************************************/
 
byte getDigit(int num, int n) {
  int int_digit, temp1, temp2;
  byte byte_digit;
 
  temp1 = pow(10, n+1);
  int_digit = num % temp1;
 
  if (n > 0) {
    temp2 = pow(10, n);
    int_digit = int_digit / temp2;
  }
 
  byte_digit = (byte) int_digit;
 
  return byte_digit;
}

void sendReading() {
  Wire.write(imu_data, 12); 
}

void TaskSensorRead(void)
{
    /* Get a new sensor event */ 
    sensors_event_t event; 
    bno.getEvent(&event);
    /* Display the floating point data */
    Serial.print("Yaw: ");
    yaw = (int) event.orientation.x;
    Serial.print(yaw);
    if (yaw < 0) {
      imu_data[8] = 1;  // Capture the sign information
      yaw = abs(yaw);
    }
    else {
      imu_data[8] = 0;
    }
    if (yaw > 360) {
      yaw = yaw - 360; // Calculate equivalent angle
    } 
     
    Serial.print("\tPitch: ");
    pitch = (int) event.orientation.y;
    Serial.print(pitch);
    if (pitch < 0) {
      imu_data[4] = 1;   // Capture the sign information
      pitch = abs(pitch);
    }
    else {
      imu_data[4] = 0;
    }
     
    Serial.print("\tRoll: ");
    roll = (int) event.orientation.z; 
    Serial.print(roll);
    if (roll < 0) {
      imu_data[0] = 1;    // Capture the sign information
      roll = abs(roll);
    }    
    else {
      imu_data[0] = 0;
    }
    Serial.println("");
    /* Update the IMU data by extracting each digit from the raw data */
    imu_data[1] = getDigit(roll, 2);
    imu_data[2] = getDigit(roll, 1);
    imu_data[3] = getDigit(roll, 0);
    imu_data[5] = getDigit(pitch, 2);
    imu_data[6] = getDigit(pitch, 1);
    imu_data[7] = getDigit(pitch, 0);
    imu_data[9] = getDigit(yaw, 2);
    imu_data[10] = getDigit(yaw, 1);
    imu_data[11] = getDigit(yaw, 0);
    delay(108);
}

void setup(void) 
{
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB, on LEONARDO, MICRO, YUN, and other 32u4 based boards.
  }
  Serial.println("Orientation Sensor Test"); Serial.println("");
  
  /* Initialise the sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }
  
  delay(1000);
    
  bno.setExtCrystalUse(true);
  Wire.begin(SLAVE_ADDRESS); // Set up the Wire library and make Arduino the slave
  /* Define the callbacks for i2c communication */
  Wire.onRequest(sendReading); // Used to specify a function when the Master requests data

}

void loop(void) 
{
  while (true){
    TaskSensorRead();
  };
}
