from gps import *
import time
import threading
import socket # for socket
import sys
import smbus
 
# Source: Donat, Wolfram. "Make a Raspberry Pi-controlled Robot :
# Building a Rover with Python, Linux, Motors, and Sensors.
# Sebastopol, CA: Maker Media, 2014. Print.
# Modified by Addison Sears-Collins
# Date April 17, 2019
# Modified by Gaurev Sharma
# Date December 12, 2021
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
gpsd = None
# for RPI version 1, use bus = smbus.SMBus(0)
bus = smbus.SMBus(1)

# This is the address we setup in the Arduino Program
SLAVE_ADDRESS = 0x04

# Create a GPS Poller class.
class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd
    gpsd = gps(mode=WATCH_ENABLE)
    self.current_value = None
    self.running = True

  def run(self):
    global gpsd
    while gpsp.running:
      gpsd.next()
if __name__ == '__main__':
  gpsp = GpsPoller()
  sock.connect((sys.argv[1], int(sys.argv[2])))
  def request_reading():
    # Read a block of 12 bytes starting at SLAVE_ADDRESS, offset 0
    reading = bus.read_i2c_block_data(SLAVE_ADDRESS, 0, 12)

    # Extract the IMU reading data
    if reading[0] < 1:
      roll_sign = "+"
    else:
      roll_sign = "-"
    roll_1 = reading[1]
    roll_2 = reading[2]
    roll_3 = reading[3]

    if reading[4] < 1:
      pitch_sign = "+"
    else:
      pitch_sign = "-"
    pitch_1 = reading[5]
    pitch_2 = reading[6]
    pitch_3 = reading[7]

    if reading[8] < 1:
      yaw_sign = "+"
    else:
      yaw_sign = "-"
    yaw_1 = reading[9]
    yaw_2 = reading[10]
    yaw_3 = reading[11]

    # Print the IMU data to the console
    print("Lat: " + str(gpsd.fix.latitude) # Log the latitude data
          + "\tLon: " + str(gpsd.fix.longitude) # Log the longitude data
          + "\tAlt: " + str(gpsd.fix.altitude / .3048) # Log the altitude in feet
          + "\nRoll: " + roll_sign + str(roll_1) + str(roll_2) + str(roll_3) +
          "   Pitch: " + pitch_sign + str(pitch_1) + str(pitch_2) + str(pitch_3) +
          "   Yaw: " + yaw_sign + str(yaw_1) + str(yaw_2) + str(yaw_3))

    try:
      sock.send("Lat: " + str(gpsd.fix.latitude) # Log the latitude data
          + "\tLon: " + str(gpsd.fix.longitude) # Log the longitude data
          + "\tAlt: " + str(gpsd.fix.altitude / .3048) # Log the altitude in feet
          + "\nRoll: " + roll_sign + str(roll_1) + str(roll_2) + str(roll_3) +
          "   Pitch: " + pitch_sign + str(pitch_1) + str(pitch_2) + str(pitch_3) +
          "   Yaw: " + yaw_sign + str(yaw_1) + str(yaw_2) + str(yaw_3))
    except(KeyboardInterrupt, SystemExit):
      print("ERROR")

  try:
    gpsp.start()
    while True:
      request_reading()
      time.sleep(5)
  except(KeyboardInterrupt, SystemExit):
    gpsp.running = False
    gpsp.join()